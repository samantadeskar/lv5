Samanta

# **Hypertext Transfer Protocol**		![alt text](http://httpwg.org/asset/http.svg)
### **http**

The Hypertext Transfer Protocol (HTTP) is an [application protocol](https://en.wikipedia.org/wiki/Application_layer) for distributed, collaborative, and [hypermedia](https://en.wikipedia.org/wiki/Hypermedia) information systems. 
HTTP is the foundation of data communication for the [World Wide Web](https://en.wikipedia.org/wiki/World_Wide_Web). Hypertext is structured text that uses logical links (hyperlinks) between nodes containing text. HTTP is the protocol to exchange or transfer hypertext.
Development of HTTP was initiated by Tim Berners-Lee at CERN in 1989. 


#### Versions of http

| Year        | Version           | 
| ------------- |:-------------:| -----:|
| 1991      | 0.9 | 
| 1996      | 1.0      |   
| 1997 | 1.1      |    
| 2015 | 2.0   |    




```html
<html>
<head>
  <title>An Example Page</title>
</head>
<body>
  Hello World, this is a very simple HTML document.
</body>
</html>
```